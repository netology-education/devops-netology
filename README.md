## Домашнее задание к занятию «2.1. Системы контроля версий» ##

### Задание №1 – Создать и настроить репозиторий для дальнейшей работы на курсе

#### Создайте репозиторий и первый коммит:

1. Зарегистрировался на https://bitbucket.org. Захотелось поработать с **BitBucket**, 
чтобы в дальнейшем, если столкнусь с ним на практике, не тратить лишнее время на адаптацию

2. https://bitbucket.org/netology-education/devops-netology/src/master/

3. `$ git clone git@bitbucket.org:netology-education/devops-netology.git`
```commandline
Клонирование в «devops-netology»…
remote: Enumerating objects: 3, done.
remote: Counting objects: 100% (3/3), done.
remote: Compressing objects: 100% (2/2), done.
remote: Total 3 (delta 0), reused 0 (delta 0), pack-reused 0
Получение объектов: 100% (3/3), готово.
```

4. `$ cd ~/git/netology/devops-netology/`

5. Уже был настроен `$ git config --global --list`
```commandline
user.name=Aleksandr Mikhaylov
user.email=peach666@gmail.com
core.autocrlf=input
core.safecrlf=warn
core.quotepath=off
color.status=auto
color.branch=auto
color.interactive=auto
color.diff=auto
pull.ff=only
```

6. `$ git status`
```commandline
На ветке master
Ваша ветка обновлена в соответствии с «origin/master».

Изменения, которые не в индексе для коммита:
  (используйте «git add <файл>…», чтобы добавить файл в индекс)
  (используйте «git restore <файл>…», чтобы отменить изменения в рабочем каталоге)
	изменено:      README.md

Неотслеживаемые файлы:
  (используйте «git add <файл>…», чтобы добавить в то, что будет включено в коммит)
	.idea/

нет изменений добавленных для коммита
(используйте «git add» и/или «git commit -a»)
```

7. Смотри пункт 6 :)

8. Смотри пункты 6 и 7 :)

9. `$ git diff`
<details>
  <summary>Нажми, чтобы развернуть вывод команды git diff</summary>
  
```commandline
diff --git a/README.md b/README.md
index 39af52c..8aa8690 100644
--- a/README.md
+++ b/README.md
@@ -1,29 +1,51 @@
-# README #
-
-This README would normally document whatever steps are necessary to get your application up and running.
-
-### What is this repository for? ###
-
-* Quick summary
-* Version
-* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)
-
-### How do I get set up? ###
-
-* Summary of set up
-* Configuration
-* Dependencies
-* Database configuration
-* How to run tests
-* Deployment instructions
-
-### Contribution guidelines ###
-
-* Writing tests
-* Code review
-* Other guidelines
-
-### Who do I talk to? ###
-
-* Repo owner or admin
-* Other community or team contact
\ No newline at end of file
+## Домашнее задание к занятию «2.1. Системы контроля версий» ##
+
+### Задание №1 – Создать и настроить репозиторий для дальнейшей работы на курсе
+
+#### Создайте репозиторий и первый коммит:
+1. Зарегистрировался на https://bitbucket.org. Захотелось поработать с **BitBucket**,
+чтобы в дальнейшем, если столкнусь с ним на практике, не тратить лишнее время на адаптацию
+2. https://bitbucket.org/netology-education/devops-netology/src/master/
+3. `$ git clone git@bitbucket.org:netology-education/devops-netology.git`
+```commandline
+Клонирование в «devops-netology»…
+remote: Enumerating objects: 3, done.
+remote: Counting objects: 100% (3/3), done.
+remote: Compressing objects: 100% (2/2), done.
+remote: Total 3 (delta 0), reused 0 (delta 0), pack-reused 0
+Получение объектов: 100% (3/3), готово.
+```
+4. `$ cd ~/git/netology/devops-netology/`
+5. Уже был настроен `$ git config --global --list`
+```commandline
+user.name=Aleksandr Mikhaylov
+user.email=peach666@gmail.com
+core.autocrlf=input
+core.safecrlf=warn
+core.quotepath=off
+color.status=auto
+color.branch=auto
+color.interactive=auto
+color.diff=auto
+pull.ff=only
+```
+6. `$ git status`
+```commandline
+На ветке master
+Ваша ветка обновлена в соответствии с «origin/master».
+
+Изменения, которые не в индексе для коммита:
+  (используйте «git add <файл>…», чтобы добавить файл в индекс)
+  (используйте «git restore <файл>…», чтобы отменить изменения в рабочем каталоге)
+       изменено:      README.md
+
+Неотслеживаемые файлы:
+  (используйте «git add <файл>…», чтобы добавить в то, что будет включено в коммит)
+       .idea/
+
+нет изменений добавленных для коммита
+(используйте «git add» и/или «git commit -a»)
+```
+7. Смотри пункт 6 :)
+8. Смотри пункты 6 и 7 :)
+9.
```
</details>

Команда `$ git diff --staged` никакого выхлопа не даёт, 
потому что я пока ещё ничего не отметил для сохранения

10. Теперь с точностью до наоборот: команда `$ git diff` не показывает никакого 
вывода, потому что изменённый **README.md** я добавил для сохранения, а команда 
`$ git diff --staged` отображает разницу между изначальным **README.md** и изменённым, 
который теперь добавлен для фиксации в коммит
<details>
  <summary>Нажми, чтобы развернуть вывод команды git diff --staged</summary>

```commandline
diff --git a/README.md b/README.md
index 39af52c..da37d4c 100644
--- a/README.md
+++ b/README.md
@@ -1,29 +1,157 @@
-# README #
+## Домашнее задание к занятию «2.1. Системы контроля версий» ##

-This README would normally document whatever steps are necessary to get your application up and running.
+### Задание №1 – Создать и настроить репозиторий для дальнейшей работы на курсе

-### What is this repository for? ###
+#### Создайте репозиторий и первый коммит:

-* Quick summary
-* Version
-* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)
+1. Зарегистрировался на https://bitbucket.org. Захотелось поработать с **BitBucket**,
+чтобы в дальнейшем, если столкнусь с ним на практике, не тратить лишнее время на адаптацию

-### How do I get set up? ###
+2. https://bitbucket.org/netology-education/devops-netology/src/master/

-* Summary of set up
-* Configuration
-* Dependencies
-* Database configuration
-* How to run tests
-* Deployment instructions
+3. `$ git clone git@bitbucket.org:netology-education/devops-netology.git`
+```commandline
+Клонирование в «devops-netology»…
+remote: Enumerating objects: 3, done.
+remote: Counting objects: 100% (3/3), done.
+remote: Compressing objects: 100% (2/2), done.
+remote: Total 3 (delta 0), reused 0 (delta 0), pack-reused 0
+Получение объектов: 100% (3/3), готово.
+```

-### Contribution guidelines ###
+4. `$ cd ~/git/netology/devops-netology/`

-* Writing tests
-* Code review
-* Other guidelines
+5. Уже был настроен `$ git config --global --list`
+```commandline
+user.name=Aleksandr Mikhaylov
+user.email=peach666@gmail.com
+core.autocrlf=input
+core.safecrlf=warn
+core.quotepath=off
+color.status=auto
+color.branch=auto
+color.interactive=auto
+color.diff=auto
+pull.ff=only
+```

-### Who do I talk to? ###
+6. `$ git status`
+```commandline
+На ветке master
+Ваша ветка обновлена в соответствии с «origin/master».

-* Repo owner or admin
-* Other community or team contact
\ No newline at end of file
+Изменения, которые не в индексе для коммита:
+  (используйте «git add <файл>…», чтобы добавить файл в индекс)
+  (используйте «git restore <файл>…», чтобы отменить изменения в рабочем каталоге)
+       изменено:      README.md
+
+Неотслеживаемые файлы:
+  (используйте «git add <файл>…», чтобы добавить в то, что будет включено в коммит)
+       .idea/
+
+нет изменений добавленных для коммита
+(используйте «git add» и/или «git commit -a»)
+```
+
+7. Смотри пункт 6 :)
+
+8. Смотри пункты 6 и 7 :)
+
+9. `$ git diff`
+<details>
+  <summary>Нажми, чтобы развернуть вывод команды git diff</summary>
+
+```commandline
+diff --git a/README.md b/README.md
+index 39af52c..8aa8690 100644
+--- a/README.md
++++ b/README.md
+@@ -1,29 +1,51 @@
+-# README #
+-
+-This README would normally document whatever steps are necessary to get your application up and running.
+-
+-### What is this repository for? ###
+-
+-* Quick summary
+-* Version
+-* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)
+-
+-### How do I get set up? ###
+-
+-* Summary of set up
+-* Configuration
+-* Dependencies
+-* Database configuration
+-* How to run tests
+-* Deployment instructions
+-
+-### Contribution guidelines ###
+-
+-* Writing tests
+-* Code review
+-* Other guidelines
+-
+-### Who do I talk to? ###
+-
+-* Repo owner or admin
+-* Other community or team contact
+\ No newline at end of file
++## Домашнее задание к занятию «2.1. Системы контроля версий» ##
++
++### Задание №1 – Создать и настроить репозиторий для дальнейшей работы на курсе
++
++#### Создайте репозиторий и первый коммит:
++1. Зарегистрировался на https://bitbucket.org. Захотелось поработать с **BitBucket**,
++чтобы в дальнейшем, если столкнусь с ним на практике, не тратить лишнее время на адаптацию
++2. https://bitbucket.org/netology-education/devops-netology/src/master/
++3. `$ git clone git@bitbucket.org:netology-education/devops-netology.git`
++```commandline
++Клонирование в «devops-netology»…
++remote: Enumerating objects: 3, done.
++remote: Counting objects: 100% (3/3), done.
++remote: Compressing objects: 100% (2/2), done.
++remote: Total 3 (delta 0), reused 0 (delta 0), pack-reused 0
++Получение объектов: 100% (3/3), готово.
++```
++4. `$ cd ~/git/netology/devops-netology/`
++5. Уже был настроен `$ git config --global --list`
++```commandline
++user.name=Aleksandr Mikhaylov
++user.email=peach666@gmail.com
++core.autocrlf=input
++core.safecrlf=warn
++core.quotepath=off
++color.status=auto
++color.branch=auto
++color.interactive=auto
++color.diff=auto
++pull.ff=only
++```
++6. `$ git status`
++```commandline
++На ветке master
++Ваша ветка обновлена в соответствии с «origin/master».
++
++Изменения, которые не в индексе для коммита:
++  (используйте «git add <файл>…», чтобы добавить файл в индекс)
++  (используйте «git restore <файл>…», чтобы отменить изменения в рабочем каталоге)
++       изменено:      README.md
++
++Неотслеживаемые файлы:
++  (используйте «git add <файл>…», чтобы добавить в то, что будет включено в коммит)
++       .idea/
++
++нет изменений добавленных для коммита
++(используйте «git add» и/или «git commit -a»)
++```
++7. Смотри пункт 6 :)
++8. Смотри пункты 6 и 7 :)
++9.
+```
+</details>
+
+Команда `$ git diff --staged` никакого выхлопа не даёт,
+потому что я пока ещё ничего не отметил для сохранения
+
+10.
```
</details>

11. Ну, допустим поиграл :)

12. Так как после команды выполнения `$ git add README.md` в пункте 10 
я вносил поверх ещё изменения в этот файл, то выполню эту команду ещё раз

13. `$ git commit -m 'First commit'`
```commandline
[master a09ad38] First commit
 1 file changed, 355 insertions(+), 29 deletions(-)
 rewrite README.md (98%)
```
`$ git diff`
```commandline
diff --git a/README.md b/README.md
index 2142040..bea83f2 100644
--- a/README.md
+++ b/README.md
@@ -353,3 +353,10 @@ index 39af52c..da37d4c 100644
 12. Так как после команды выполнения `$ git add README.md` в пункте 10
 я вносил поверх ещё изменения в этот файл, то выполню эту команду ещё раз

+13. `$ git commit -m 'First commit'`
+```commandline
+[master a09ad38] First commit
+ 1 file changed, 355 insertions(+), 29 deletions(-)
+ rewrite README.md (98%)
+```
+
```
Команда `$ git diff --staged` никакого вывода не даёт, так как файл **README.md** предыдущими 
действиями я добавил для фиксации и зафиксировал изменения в нём

---

#### Создадим файлы .gitignore и второй коммит:

1. Создал **.gitignore** с содержимым `$ echo -e '.idea/\n' > .gitignore`

2. Пишу постоянно свои ответы и действия сюда, в **README.md**, поэтому включу 
и его в коммит тоже
`$ git status`

```commandline
На ветке master
Ваша ветка опережает «origin/master» на 1 коммит.
  (используйте «git push», чтобы опубликовать ваши локальные коммиты)

Изменения, которые не в индексе для коммита:
  (используйте «git add <файл>…», чтобы добавить файл в индекс)
  (используйте «git restore <файл>…», чтобы отменить изменения в рабочем каталоге)
	изменено:      README.md

Неотслеживаемые файлы:
  (используйте «git add <файл>…», чтобы добавить в то, что будет включено в коммит)
	.gitignore

нет изменений добавленных для коммита
(используйте «git add» и/или «git commit -a»)
```

```commandline
$ git add .

$ git status

На ветке master
Ваша ветка опережает «origin/master» на 1 коммит.
  (используйте «git push», чтобы опубликовать ваши локальные коммиты)

Изменения, которые будут включены в коммит:
  (используйте «git restore --staged <файл>…», чтобы убрать из индекса)
	новый файл:    .gitignore
	изменено:      README.md
```

3. Прошёл по ссылке, скопировал в буфер обмена всё содержимое файла. 
`$ mkdir terraform && pbpaste > terraform/.gitignore` --> 
Создаю каталог **terraform**, в случае успешного выполнения команды создания 
каталога вставлю содержимое буфера обмена в файл **.gitignore** внутри только 
что созданного каталога **terraform**

4. Будут проигнорированы:
* скрытый каталог **.idea/** в корне и всё содержимое этого каталога,
* все файлы, перечисленные в **terraform/.gitignore**, но только 
в пределах поддиректории **terraform/**

5. 
```commandline
$ git add .

$ git status

На ветке master
Ваша ветка опережает «origin/master» на 1 коммит.
  (используйте «git push», чтобы опубликовать ваши локальные коммиты)

Изменения, которые будут включены в коммит:
  (используйте «git restore --staged <файл>…», чтобы убрать из индекса)
	новый файл:    .gitignore
	изменено:      README.md
	новый файл:    terraform/.gitignore

$ git commit -m 'Added gitignore'

[master bcb3978] Added gitignore
 3 files changed, 117 insertions(+)
 create mode 100644 .gitignore
 create mode 100644 terraform/.gitignore
```

---

#### Экспериментируем с удалением и перемещением файлов (третий и четвертый коммит)

1. 
```commandline
$ echo 'will_be_deleted' > will_be_deleted.txt; echo 'will_be_moved' > will_be_moved.txt

$ git status

На ветке master
Ваша ветка опережает «origin/master» на 2 коммита.
  (используйте «git push», чтобы опубликовать ваши локальные коммиты)

Изменения, которые не в индексе для коммита:
  (используйте «git add <файл>…», чтобы добавить файл в индекс)
  (используйте «git restore <файл>…», чтобы отменить изменения в рабочем каталоге)
	изменено:      README.md

Неотслеживаемые файлы:
  (используйте «git add <файл>…», чтобы добавить в то, что будет включено в коммит)
	will_be_deleted.txt
	will_be_moved.txt

нет изменений добавленных для коммита
(используйте «git add» и/или «git commit -a»)

$ git add .

$ git commit -m 'Prepare to delete and move'

[master d3c87c8] Prepare to delete and move
 3 files changed, 26 insertions(+)
 create mode 100644 will_be_deleted.txt
 create mode 100644 will_be_moved.txt
```

2. –

3. 
```commandline
$ rm will_be_deleted.txt

$ git status

На ветке master
Ваша ветка опережает «origin/master» на 3 коммита.
  (используйте «git push», чтобы опубликовать ваши локальные коммиты)

Изменения, которые не в индексе для коммита:
  (используйте «git add/rm <файл>…», чтобы добавить или удалить файл из индекса)
  (используйте «git restore <файл>…», чтобы отменить изменения в рабочем каталоге)
	изменено:      README.md
	удалено:       will_be_deleted.txt

нет изменений добавленных для коммита
```

4. 
```commandline
$ mv will_be_moved.txt has_been_moved.txt

$ git status

На ветке master
Ваша ветка опережает «origin/master» на 3 коммита.
  (используйте «git push», чтобы опубликовать ваши локальные коммиты)

Изменения, которые не в индексе для коммита:
  (используйте «git add/rm <файл>…», чтобы добавить или удалить файл из индекса)
  (используйте «git restore <файл>…», чтобы отменить изменения в рабочем каталоге)
	изменено:      README.md
	удалено:       will_be_deleted.txt
	удалено:       will_be_moved.txt

Неотслеживаемые файлы:
  (используйте «git add <файл>…», чтобы добавить в то, что будет включено в коммит)
	has_been_moved.txt

нет изменений добавленных для коммита
(используйте «git add» и/или «git commit -a»)
```

5. 
```commandline
$ git add .

$ git status

На ветке master
Ваша ветка опережает «origin/master» на 3 коммита.
  (используйте «git push», чтобы опубликовать ваши локальные коммиты)

Изменения, которые будут включены в коммит:
  (используйте «git restore --staged <файл>…», чтобы убрать из индекса)
	изменено:      README.md
	переименовано: will_be_moved.txt -> has_been_moved.txt
	удалено:       will_be_deleted.txt

$ git commit -m 'Moved and deleted'

[master 845f9db] Moved and deleted
 3 files changed, 83 insertions(+), 1 deletion(-)
 rename will_be_moved.txt => has_been_moved.txt (100%)
 delete mode 100644 will_be_deleted.txt
```

---

#### Проверка изменений


```commandline
$ git log --pretty=oneline

845f9db86672675465c29ba319842b70a7693acd (HEAD -> master) Moved and deleted
d3c87c8ffcb094f20e546ea82fb2bbb54616c54a Prepare to delete and move
bcb397860aa678aec26ba11086e4eb65c24cd93a Added gitignore
a09ad382496d6ef743cac71bfb96e9e72e57eaff First commit
38239984dce0da8aa433bc076956df963bdf04ac (origin/master, origin/HEAD) Initial commit
```

<details>
  <summary>Нажми, чтобы развернуть вывод команды git log</summary>

```commandline
$ git log

commit 845f9db86672675465c29ba319842b70a7693acd (HEAD -> master)
Author: Aleksandr Mikhaylov <peach666@gmail.com>
Date:   Sun Apr 3 02:05:43 2022 +0600

    Moved and deleted

commit d3c87c8ffcb094f20e546ea82fb2bbb54616c54a
Author: Aleksandr Mikhaylov <peach666@gmail.com>
Date:   Sun Apr 3 01:56:25 2022 +0600

    Prepare to delete and move

commit bcb397860aa678aec26ba11086e4eb65c24cd93a
Author: Aleksandr Mikhaylov <peach666@gmail.com>
Date:   Sun Apr 3 01:51:03 2022 +0600

    Added gitignore

commit a09ad382496d6ef743cac71bfb96e9e72e57eaff
Author: Aleksandr Mikhaylov <peach666@gmail.com>
Date:   Sun Apr 3 01:03:21 2022 +0600

    First commit

commit 38239984dce0da8aa433bc076956df963bdf04ac (origin/master, origin/HEAD)
Author: monk_22 <peach666@gmail.com>
Date:   Sat Apr 2 18:06:02 2022 +0000

    Initial commit
```
</details>
